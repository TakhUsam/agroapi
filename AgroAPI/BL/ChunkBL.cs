﻿using AgroAPI.Utils.FileManager;
using Microsoft.EntityFrameworkCore;

public class ChunkService
{
    private readonly AppDbContext _dbContext;
    private readonly IFileManager _fileManager;

    public ChunkService(AppDbContext dbContext, IFileManager fileManager)
    {
        _dbContext = dbContext;
        _fileManager = fileManager;
    }

    public async Task<Chunk?> GetChunkByIdAsync(int id)
    {
        return await _dbContext.Chunks.FindAsync(id);
    }

    public async Task<List<Chunk>> GetAllChunksAsync()
    {
        return await _dbContext.Chunks.ToListAsync();
    }

    public async Task CreateChunkAsync(Chunk chunk)
    {
        chunk.ImageUrl = await _fileManager.SaveFileAsync(chunk.File);
        await _dbContext.Chunks.AddAsync(chunk);
        await _dbContext.SaveChangesAsync();
    }

    public async Task DeleteChunkByIdAsync(int id)
    {
        var chunk = await _dbContext.Chunks.FindAsync(id);
        if (chunk == null)
        {
            throw new ArgumentException("Объект не найдена", nameof(id));
        }

        _dbContext.Chunks.Remove(chunk);
        await _dbContext.SaveChangesAsync();
        await _fileManager.DeleteFileAsync(chunk.ImageUrl);
    }
}