﻿using System.ComponentModel.DataAnnotations;

namespace AgroAPI.Utils
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class FileAllowedExtensionsAttribute : ValidationAttribute
    {
        private List<string> AllowedExtensions { get; set; }
        private new string ErrorMessage { get; set; }

        public FileAllowedExtensionsAttribute(string FileExtensions, string ErrorMessage)
        {
            AllowedExtensions = FileExtensions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            this.ErrorMessage = ErrorMessage;
        }


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IFormFile? file = value as IFormFile;

            if (file != null)
            {
                var fileName = file.FileName;

                var isAllowed = AllowedExtensions.Any(y => fileName.EndsWith(y));
                if (isAllowed)
                    return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage);
        }

    }

}

