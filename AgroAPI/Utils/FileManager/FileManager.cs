﻿using AgroAPI.Utils.FileManager;

public class FileManager : IFileManager
{
    private readonly string _fileDirectory;
    private readonly string _fileBaseUrl;

    public FileManager(IWebHostEnvironment webHostEnvironment, string baseUrl, string folderToSave = "files")
    {
        _fileDirectory = Path.Combine(webHostEnvironment.WebRootPath, folderToSave);
        _fileBaseUrl = baseUrl + "/" + folderToSave;

        if (!Directory.Exists(_fileDirectory))
        {
            Directory.CreateDirectory(_fileDirectory);
        }
    }

    public async Task<string> SaveFileAsync(IFormFile file)
    {
        string fileName = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
        string filePath = Path.Combine(_fileDirectory, fileName);
        using (var stream = new FileStream(filePath, FileMode.Create))
        {
            await file.CopyToAsync(stream);
        }
        return $"{_fileBaseUrl}/{fileName}";
    }


    public async Task<FileStream> GetFileAsync(string url)
    {
        string fileName = Path.GetFileName(url);
        string filePath = Path.Combine(_fileDirectory, fileName);

        return new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true);
    }


    public async Task DeleteFileAsync(string url)
    {
        string fileName = Path.GetFileName(url);
        string filePath = Path.Combine(_fileDirectory, fileName);

        await Task.Run(() => File.Delete(filePath));
    }
}