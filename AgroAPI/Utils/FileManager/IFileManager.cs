﻿namespace AgroAPI.Utils.FileManager
{
    public interface IFileManager
    {
        Task<string> SaveFileAsync(IFormFile file);

        Task<FileStream> GetFileAsync(string url);

        Task DeleteFileAsync(string url);
    }
}
