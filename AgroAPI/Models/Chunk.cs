﻿using AgroAPI.Utils;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

public class Chunk
{
    public int Id { get; set; }

    [Range(-90.0, 90.0, ErrorMessage = "Широта должна быть между -90 и 90.")]
    public double Latitude { get; set; }

    [Range(-180.0, 180.0, ErrorMessage = "Долгота должна быть между -180 и 180.")]
    public double Longitude { get; set; }

    public string ImageUrl { get; set; }

    public string? JsonData { get; set; }


    [NotMapped]
    [Required(ErrorMessage = "Файл не загружен.")]
    [JsonIgnore]
    [FileAllowedExtensions(FileExtensions:".png,.jpg,.jpeg", ErrorMessage : "Поддерживаются только форматы png, jpg, jpeg")]
    public IFormFile File { get; set; }

}