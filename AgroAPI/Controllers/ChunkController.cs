﻿using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/chunks")]
public class ChunkController : ControllerBase
{
    private readonly ChunkService _chunkService;

    public ChunkController(ChunkService chunkService)
    {
        _chunkService = chunkService;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Chunk>> GetChunkById(int id)
    {
        var model = await _chunkService.GetChunkByIdAsync(id);
        if (model == null)
        {
            return NotFound();
        }

        return Ok(model);
    }

    [HttpGet]
    public async Task<ActionResult<List<Chunk>>> GetAllChunks()
    {
        var models = await _chunkService.GetAllChunksAsync();
        return Ok(models);
    }

    [HttpPost]
    public async Task<IActionResult> CreateChunkAsync([FromForm] Chunk chunk)
    {
        await _chunkService.CreateChunkAsync(chunk);
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteChunkById(int id)
    {
        try
        {
            await _chunkService.DeleteChunkByIdAsync(id);
            return Ok();
        }
        catch (ArgumentException)
        {
            return NotFound();
        }
    }
}